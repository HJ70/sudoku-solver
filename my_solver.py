from sudoku import Sudoku


class MySolver:

    def __init__(self, sudoku: Sudoku):
        self.sudoku = sudoku

    def x_solve(self):
        self.sudoku.general_trimmer()


    @staticmethod
    def solve(sudoku: str) -> str:
        my_sudoku = Sudoku(sudoku)
        solver = MySolver(my_sudoku)
        solver.x_solve()
        return my_sudoku.encode()
