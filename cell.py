from termcolor import colored

DEFAULT_COLOR = "white"
INITIAL_FIXED_COLOR = "blue"
FIXED_COLOR = "green"
CHANGED_COLOR = "yellow"
NEW_FIXED_COLOR = "red"


class Cell:

    def __init__(self, data: set[int], initial_fixed: bool = False):
        self.data = data
        self.color = INITIAL_FIXED_COLOR if initial_fixed else DEFAULT_COLOR

    def __str__(self):
        return f"{self.data}"

    def __repr__(self):
        return str(self)

    def get_data(self) -> set[int]:
        return self.data

    def get(self) -> int:
        return next(iter(self.data))

    def is_fixed(self) -> bool:
        return len(self.data) == 1

    def find_pair(self) -> set[int] or None:
        return self.find_set(2)

    def find_set(self, size: int) -> set[int] or None:
        if len(self.data) == size:
            return self.data
        else:
            return None

    def get_quartet(self) -> set[int] or None:
        if len(self.data) == 4:
            return self.data
        else:
            return None

    def remove_number(self, del_num: int) -> bool:
        if del_num in self.data and not self.is_fixed():
            self.data.discard(del_num)
            if self.is_fixed():
                self.set_fixed_color()
            else:
                self.set_changed_color()
            return True

        return False

    def get_fixed(self) -> int:
        return next(iter(self.data))

    def count(self) -> int:
        return len(self.data)

    def encode(self, show_candidates=True) -> str:
        if self.is_fixed():
            return str(self.get())

        if show_candidates:
            result = ""
            for n in self.data:
                result += str(n)
            return result

        return ""

    def set_fixed_color(self):
        self.color = NEW_FIXED_COLOR

    def set_changed_color(self):
        self.color = CHANGED_COLOR

    def reset_colors(self):
        if self.color == CHANGED_COLOR:
            self.color = DEFAULT_COLOR
        if self.color == NEW_FIXED_COLOR:
            self.color = FIXED_COLOR

    def set_new_fixed(self, new_fixed_num: int):
        self.data.clear()
        self.data.add(new_fixed_num)
        self.set_fixed_color()
