from cell import Cell


class Board:
    def __init__(self, dim):
        self.dim = dim
        self.full_cell = {x for x in range(1, dim + 1)}
        self.content: list[list[Cell]] = self.initialized_grid()

    def initialized_grid(self) -> list[list[Cell]]:
        return [[Cell(self.full_cell) for _ in range(self.dim)] for _ in
                range(self.dim)]

    def get(self, row: int, col: int) -> Cell:
        return self.content[row][col]

    def set(self, row: int, col: int, cell: Cell):
        self.content[row][col] = cell

    def reduce(self, row: int, col: int, number: int) -> bool:
        cell = self.get(row, col)
        return cell.reduce(number)

    def fixate(self, row: int, col: int, number: int):
        self.set(row, col, Cell({number}))

    def max_candidates(self):
        max_candidates = 0
        for row in range(self.dim):
            for col in range(self.dim):
                candidates = self.get(row, col).candidates()
                max_candidates = max(candidates, max_candidates)
        return max_candidates
