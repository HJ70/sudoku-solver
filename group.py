import cell
from grid import Grid
from cell_coordinates import CellCoordinates


class Group:

    def __init__(self):
        self.data: list[CellCoordinates] = []

    def __repr__(self):
        return str(self.data)

    def find_set_in_cells(self, check_set: set[int], grid: Grid,
                          cell_coordinates_to_skip: set[CellCoordinates]) -> \
            CellCoordinates | None:
        for coordinates in self.data:
            if coordinates in cell_coordinates_to_skip:
                continue
            cell = grid.get_cell(coordinates)
            if cell.data == check_set:
                return coordinates

        return None

    def occurs_once(self, num) -> bool:
        for cell_coordinate in self.data:
            cell = self.grid.get_cell(cell_coordinate)
            if self.data.count(num) == 1:
                return True

    def delete_num_in_cells(self,
                            del_num: int,
                            grid: Grid,
                            cell_coordinates_dont_del: set[
                                CellCoordinates]) -> bool:
        removed: bool = False
        for coordinates in self.data:
            if coordinates in cell_coordinates_dont_del:
                continue
            cell = grid.get_cell(coordinates)
            removed |= cell.remove_number(del_num)

        return removed

    def delete_pair_in_cells(self,
                             pair: set[int],
                             grid: Grid,
                             cell_coordinates_dont_del: set[
                                 CellCoordinates]) -> bool:
        removed: bool = False
        for del_num in iter(pair):
            removed |= self.delete_num_in_cells(del_num,
                                                grid,
                                                cell_coordinates_dont_del)

        return removed

    def delete_in_cells(self,
                        check_set: set[int],
                        grid: Grid,
                        cell_coords_to_skip: set[
                            CellCoordinates]) -> bool:
        removed: bool = False
        for num in iter(check_set):
            removed |= self.delete_num_in_cells(num,
                                                grid,
                                                cell_coords_to_skip)

        return removed

    def find_cell_where_num_is_hidden_single(self, number: int, grid: Grid) -> CellCoordinates or None:
        for cell_coordinate in self.data:
            cell = grid.get_cell(cell_coordinate)
            if number in cell.data:
                if cell.is_fixed():
                    continue
                found_in_other_cell: bool = False
                for other_cell_coordinate in self.data:
                    if cell_coordinate == other_cell_coordinate:
                        continue
                    other_cell = grid.get_cell(other_cell_coordinate)

                    if number in other_cell.data:
                        found_in_other_cell = True
                        break

                if found_in_other_cell:
                    continue

                return cell_coordinate

            return None

