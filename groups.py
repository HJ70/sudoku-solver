from group import Group
from board import Board


class Groups:

    def __init__(self, dim, box_size):
        self.content = []

        # add a group for every row/col
        for i in range(dim):
            # row
            self.append(Group([(i, j) for j in range(dim)]))
            # col
            self.append(Group([(j, i) for j in range(dim)]))

        # add a group for both diagonals
        # self.append(Group([(i, i) for i in range(dim)]))
        # self.append(Group([(i, dim - 1 - i) for i in range(dim)]))

        # add a group for the boxes
        for row_offset in range(int(dim / box_size)):
            for col_offset in range(int(dim / box_size)):
                self.append(Group([(row_offset * box_size + ii,
                                    col_offset * box_size + jj)
                                   for ii in range(box_size) for jj in
                                   range(box_size)]))

    def append(self, group):
        self.content.append(group)

    def condense(self, board: Board):
        while True:  # repeat until no further improvements
            fixated = False
            for group in self.content:
                fixated |= group.condense(board)
            if not fixated:
                break
