import turtle
from cell import Cell

class View:

    def __init__(self, dim: int, box_size: int):
        self.dim = dim
        self.box_size = box_size
        self.myPen = turtle.Turtle()

        self.myPen.speed(0)
        self.myPen.color("#000000")
        self.myPen.hideturtle()
        self.topLeft_x = -150
        self.topLeft_y = 150

    def text(self, message, x, y, size):
        font = ('Arial', size, 'normal')
        self.myPen.penup()
        self.myPen.goto(x, y)
        self.myPen.write(message, align="left", font=font)

    def draw_grid(self, grid: list[list[int]]):
        int_dim = 35
        for row in range(0, self.dim+1):
            if (row % self.box_size) == 0:
                self.myPen.pensize(3)
            else:
                self.myPen.pensize(1)
            self.myPen.penup()
            self.myPen.goto(self.topLeft_x, self.topLeft_y - row * int_dim)
            self.myPen.pendown()
            self.myPen.goto(self.topLeft_x + 9 * int_dim,
                            self.topLeft_y - row * int_dim)
        for col in range(0, self.dim+1):
            if (col % self.box_size) == 0:
                self.myPen.pensize(3)
            else:
                self.myPen.pensize(1)
            self.myPen.penup()
            self.myPen.goto(self.topLeft_x + col * int_dim, self.topLeft_y)
            self.myPen.pendown()
            self.myPen.goto(self.topLeft_x + col * int_dim,
                            self.topLeft_y - 9 * int_dim)

        for row in range(0, self.dim):
            for col in range(0, self.dim):
                cell: Cell = grid[row][col]
                cell_str = cell.encode(show_candidates=False)
                if cell_str:
                    self.text(cell_str, self.topLeft_x + col * int_dim + 9,
                              self.topLeft_y - row * int_dim - int_dim + 8, 18)

    def draw(self, grid: list[list[int]]):
        self.myPen.clear()
        self.draw_grid(grid)
        self.myPen.getscreen().update()
