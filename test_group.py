import unittest

from grid import Grid
from group import Group
from cell import Cell
from cell_coordinates import CellCoordinates

CELL_0_1 = CellCoordinates(0, 1)
CELL_0_0 = CellCoordinates(0, 0)


class TestGroup(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.grid = Grid(2)
        self.grid.data[0][1] = Cell({1, 3, 4})
        self.grid.data[1][0] = Cell({1, 3, 4})
        self.grid.data[1][1] = Cell({1, 3, 4})

    def test_have_found(self):
        group = Group()
        group.data.append(CELL_0_0)
        group.data.append(CELL_0_1)

        cell_coordinate = group.find_cell_where_num_is_hidden_single(2, self.grid)

        self.assertTrue(cell_coordinate)
        self.assertEqual(cell_coordinate.row, CELL_0_0.row)
        self.assertEqual(cell_coordinate.col, CELL_0_0.col)

    def test_have_not_found(self):
        group = Group()
        group.data.append(CELL_0_0)
        group.data.append(CELL_0_1)

        cell_coordinate = group.find_cell_where_num_is_hidden_single(1, self.grid)

        self.assertFalse(cell_coordinate)


if __name__ == '__main__':
    unittest.main()
