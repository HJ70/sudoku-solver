from time import sleep

from grid import Grid
from gui.view import View


class App:

    def run(self):
        sudoku = \
            "700900000600030000000070100002000459000000600800004000025008010008300040060702500"
        grid = Grid(sudoku)
        view = View(9, 3)

        view.draw(grid.board.content)

        while True:
            pass


if __name__ == '__main__':
    app = App()
    app.run()
