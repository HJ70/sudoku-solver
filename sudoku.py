from math import isqrt

from cell import Cell
from rules import Rules
from group import Group
from cell_coordinates import CellCoordinates
from grid import Grid
from termcolor import colored

SPACE_COUNT_PER_CELL = 3


class Sudoku:
    def __init__(self, sudoku: str):
        self.rules = Rules()
        box_dim = self.calculate_box_dim(sudoku)
        self.dim = box_dim * box_dim
        self.grid = Grid(self.dim)
        self.box_dim = box_dim
        self.create_rules()
        self.apply(sudoku)
        print("\napply")
        self.print()

    def create_rules(self):
        self.create_rules_internal(True)
        self.create_rules_internal(False)

        self.create_box_rules()

    def create_rules_internal(self, for_row: bool):
        for i in range(self.dim):
            rule = Group()
            for j in range(self.dim):
                rule.data.append(CellCoordinates(i, j) if for_row else
                                 CellCoordinates(j, i))

            self.rules.data.append(rule)

    def create_box_rule(self, offset_row: int, offset_col: int):
        group = Group()
        for i in range(self.box_dim):
            for j in range(self.box_dim):
                group.data.append(CellCoordinates(i + offset_row,
                                                  j + offset_col))

        self.rules.data.append(group)

    def create_box_rules(self):
        for i in range(self.box_dim):
            for j in range(self.box_dim):
                self.create_box_rule(self.box_dim * i, self.box_dim * j)

    def create_line(self) -> str:
        result = ""
        for i in range(self.dim * SPACE_COUNT_PER_CELL + self.box_dim + 1):
            result += "-"

        return result

    def format(self, compact=False) -> str:
        result = ""

        if not compact:
            result += self.create_line()
        for row in range(self.dim):
            if not compact:
                result += "\n|"
            for col in range(self.dim):
                cell = self.grid.get_cell(CellCoordinates(row, col))
                if compact:
                    if cell.count() == 1:
                        result += str(cell.get_fixed())
                    else:
                        result += "0"
                else:
                    if cell.count() == 1:
                        result += str(cell.get_fixed())
                    else:
                        result += "*"
                if not compact and (col + 1) % self.box_dim == 0:
                    result += "|"
            if not compact and (row + 1) % self.box_dim == 0:
                result += "\n" + self.create_line()
        return result

    def apply(self, sudoku: str) -> None:
        # 700900000600030000000070100002000459000000600800004000025008010008300040060702500
        # sd = sudoku[0:10]

        # c |  |  |  |
        # r ----------

        for r in range(self.dim):
            row = sudoku[r * self.dim: r * self.dim + self.dim]
            for c in range(self.dim):
                number = row[c]
                if number != "0":
                    self.grid.data[r][c] = Cell({int(number)}, True)

    @staticmethod
    def calculate_box_dim(sudoku: str) -> int:
        return isqrt(isqrt(len(sudoku)))

    def __str__(self):
        return self.encode(cell_sep=" ", line_sep="\n", unfinished=" ",
                           show_grid=True, show_candidates=True,
                           show_colored=True)

    def __repr__(self):
        return self.encode(cell_sep=" ",
                           line_sep="\n",
                           unfinished=" ",
                           show_grid=True,
                           show_candidates=True,
                           show_colored=False)

    def candidates(self) -> str:
        return self.encode(cell_sep=" ", line_sep="\n", show_grid=True,
                           show_candidates=True)

    def encode(self, cell_sep="", line_sep="", unfinished="0",
               show_grid=False, show_candidates=False, show_colored=False):
        max_candidates = self.grid.max_candidates() if show_candidates else 1
        result = ""
        for i in range(self.dim):
            if show_grid and i % self.box_dim == 0:
                result = self.add_line(cell_sep,
                                       result,
                                       max_candidates,
                                       self.dim)
                result += "\n"
            for j in range(self.dim):
                if show_grid and j % self.box_dim == 0:
                    result += "|"
                else:
                    result += cell_sep
                cell: Cell = self.grid.get_cell(CellCoordinates(i, j))
                cell_content = \
                    cell.encode() if show_candidates or cell.is_fixed() else \
                        unfinished

                cell_content_ljust = cell_content.ljust(max_candidates)
                if show_colored:
                    result += colored(cell_content_ljust,
                                      cell.color)
                else:
                    result += cell_content_ljust

            if show_grid:
                result += "|"
                result += line_sep

        if show_grid:
            result = self.add_line(cell_sep, result, max_candidates, self.dim)
        return result

    @staticmethod
    def add_line(cell_sep: str,
                 result: str,
                 max_candidates: int,
                 dim: int) -> str:
        for _ in range(dim * max_candidates + dim * len(cell_sep) + 1):
            result += "-"
        return result

    def reset_colors(self):
        [self.grid.get_cell(CellCoordinates(row, col)).reset_colors() for row
         in range(self.dim) for col in range(self.dim)]

    def trim_size(self, size: int, debug: bool = False) -> bool:
        # if debug:
        #    print(f"trim_size({size})")

        removed_here: bool = False

        handled_cells_by_group = {}
        for group in self.rules.data:
            handled_cells_by_group[group] = set()

        while True:
            removed: bool = False

            for group in self.rules.data:
                handled_cells: set[CellCoordinates] = handled_cells_by_group[group]
                for cell_coords in group.data:
                    if cell_coords in handled_cells:
                        continue
                    cell = self.grid.get_cell(cell_coords)
                    check_set = cell.find_set(size)
                    if not check_set:
                        continue

                    coords_to_skip = {cell_coords}

                    for i in range(len(check_set) - 1):
                        other_cell_coords = group.find_set_in_cells(check_set,
                                                                    self.grid,
                                                                    coords_to_skip)
                        if not other_cell_coords:
                            break

                        coords_to_skip.add(other_cell_coords)

                    if len(coords_to_skip) != len(check_set):
                        continue

                    handled_cells.update(coords_to_skip)

                    removed_local = group.delete_in_cells(
                        check_set,
                        self.grid,
                        coords_to_skip)

                    removed |= removed_local

                    if debug and len(check_set) > 1 and removed_local:
                        print("Set found:", check_set)

            removed_here |= removed

            if not removed:
                break

        if debug and removed_here:
            self.print()

        return removed_here

    def print(self):
        print(self)
        self.reset_colors()

    def general_trimmer(self):
        debug = True
        self.trim_size(1, debug=debug)

        while True:
            removed: bool = False
            for size in range(self.dim - 1, 0, -1):
                removed |= self.trim_size(size, debug=debug)

            removed |= self.fix_hidden_single(debug)

            if not removed:
                break

    def fix_hidden_single(self, debug=False) -> bool:
        removed: bool = False
        for group in self.rules.data:
            for number in range(1, self.dim + 1):
                possible_hidden_single_coords: CellCoordinates = group.find_cell_where_num_is_hidden_single(number, self.grid)
                if possible_hidden_single_coords:
                    possible_hidden_single = self.grid.get_cell(possible_hidden_single_coords)
                    possible_hidden_single.set_new_fixed(number)
                    removed = True
                    self.trim_size(1, debug)
                    if debug:
                        self.print()

        return removed
