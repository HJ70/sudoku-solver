from cell import Cell
from cell_coordinates import CellCoordinates


class Grid:

    def __init__(self, dim: int):
        self.dim = dim
        self.data = [[Cell({x + 1 for x in range(dim)}) for _ in range(dim)] for _ in range(dim)]

    def __repr__(self):
        return str(self.data)

    def __str__(self):
        return str(self.data)

    def get_cell(self, cell_coordinates: CellCoordinates):
        return self.data[cell_coordinates.row][cell_coordinates.col]

    def max_candidates(self):
        max_candidates_count = 0
        for row in range(self.dim):
            for col in range(self.dim):
                candidates_count = \
                    self.get_cell(CellCoordinates(row, col)).count()
                max_candidates_count = \
                    max(candidates_count, max_candidates_count)

        return max_candidates_count
